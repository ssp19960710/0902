from typing import List, Union, Optional
from gitlab import Gitlab, GitlabError

class GitlabManager:
    def __init__(self, token: str, base_url: str, project_id: int):
        self.gl = Gitlab(base_url, private_token=token)
        self.project = self.gl.projects.get(project_id)
        self.branch_names = {branch.name for branch in self.project.branches.list()}

    def _validate_path(self, path: str):
        if not path:
            raise ValueError("Path cannot be empty")

    def _validate_branch(self, branch: str):
        if branch not in self.branch_names:
            raise ValueError(f"Branch '{branch}' does not exist in the project")

    def _get_file(self, path: str, branch: str = None) -> Optional[str]:
        try:
            return self.project.files.get(file_path=path, ref=branch).decode().decode()
        except GitlabError as e:
            print(f"Error retrieving file {path}: {e}")
            return None

    def _check_content_match(self, existing_content: str, new_content: str) -> bool:
        return existing_content is not None and existing_content == new_content

    def commit_files_to_gitlab(self, files: List[dict], commit_message: str, branch: str = "main") -> bool:
        self._validate_branch(branch)
        actions = []

        try:
            for file_info in files:
                file_path = file_info['file_path']
                content = file_info['content']
                existing_content = self._get_file(file_path, branch)

                if self._check_content_match(existing_content, content):
                    print(f"Skipping commit for file '{file_path}' as content matches.")
                    continue

                actions.append({'action': 'update' if existing_content is not None else 'create',
                                'file_path': file_path, 'content': content})

            if not actions:
                print("No files need to commit.")
                return False

            self.project.commits.create({'actions': actions, 'branch': branch, 'commit_message': commit_message})
            return True

        except GitlabError as e:
            print(f"Error committing files to GitLab: {e}")
            return False

